import moment from "moment";

const { createSlice } = require("@reduxjs/toolkit")

let initialState={
    isStartSearch:false,
    place:{viTri:"",maViTri:''},
    checkIn:'',
    checkOut:'',
    guest:"",
    listRoom:[],
    roomChoose:{},
}
let SearchSlice=createSlice({
    name:"SearchSlice",
    initialState,
    reducers:{
        setPlace:(state,{payload})=>{
            console.log('setPlace');
            state.place={...state.place,...payload}
        },
        setCheckIn:(state,{payload})=>{

            console.log('setCheckIn',moment(payload).format('DD MM YYYY'));
         
            // state.checkIn=moment(payload).format("YYYY-MM-DD");
            state.checkIn=payload;
        },
        setCheckOut:(state,{payload})=>{
            console.log('setCheckOut');
            // state.checkOut=moment(payload).format("YYYY-MM-DD");
            state.checkOut=payload
        },
        setGuest:(state,{payload})=>{
            state.guest=payload;
        },
        setIsStartSearch:(state,{payload})=>{
            // console.log('setIsStartSearch');
            state.isStartSearch=payload
        },
        setListRoom:(state,{payload})=>{
            console.log('setlistrooombooked');
            state.listRoom=payload
        },
        setRoomChoose:(state,{payload})=>{
            state.roomChoose=payload
        }
    }
});
export let{setPlace,setCheckIn,setCheckOut,setIsStartSearch,setListRoom,setGuest,setRoomChoose}=SearchSlice.actions;
export default SearchSlice.reducer;