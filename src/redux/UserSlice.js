const { createSlice } = require("@reduxjs/toolkit");
const { localService } = require("../services/local.service");

let initialState={
    userInfo:localService.get(),
    isPopUpLogin:false,
    isPopUpBooking:false,
}
 let UserSlice=createSlice({
    name:"UserSlice",
    initialState,
    reducers:{
        setUserInfor:(state,{payload})=>{
            state.userInfo=payload;
        },
        setIsPopUpLogin:(state,{payload})=>{
            state.isPopUpLogin=payload;
        },
        setIsPopUpBooking:(state,{payload})=>{
            state.isPopUpBooking=payload;
        }
    }
 })
 export let {setUserInfor,setIsPopUpLogin,setIsPopUpBooking}=UserSlice.actions;
 export default UserSlice.reducer;