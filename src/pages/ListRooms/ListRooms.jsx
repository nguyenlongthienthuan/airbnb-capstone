import moment from 'moment';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import { setListRoom, setPlace } from '../../redux/SearchSlice';
import { placesService } from '../../services/place.service';

function ListRooms() {
    let dispatch=useDispatch();
    let params=useParams();
    let navigate=useNavigate();
    // let search=useSelector((state)=>{
    //     return state.UserSlice.placeChoose;
    // })
    // const [listRooms,setListRoom]=useState([])
    // const search=useSelector((state)=>{
    //     return state.SearchSlice;
    // })
    const checkIn=useSelector((state)=>{
        return state.SearchSlice.checkIn;
    })
    const checkOut=useSelector((state)=>{
        return state.SearchSlice.checkOut;
    })
    const place=useSelector((state)=>{
        return state.SearchSlice.place;
    })
    const [viTri,setViTri]=useState('');
    // console.log(place);
    // let {place,checkIn,checkOut}=search;
    // console.log('place ne',place);
   
    const listRoom=useSelector(
        (state)=>{
            return state.SearchSlice.listRoom;
        }
    )
    let {id}=params;
    useEffect(()=>{
        window.scrollTo(0, 0);
        placesService.getPhongTheoViTri({maViTri:parseInt(id)}).then(
            (res)=>{
                console.log("data list phong ",res.data.content);
                let danhSachPhong=res.data.content;
                if(checkIn=="" || checkOut==""){
                    // console.log("ko co time");
                    dispatch(setListRoom(res.data.content))
                }else{
                    console.log('co time');
                    placesService.getDatPhong().then((res)=>{
                        let danhSachPhongDaDat=res.data.content;
                        dispatch(setListRoom(danhSachPhong.filter((item)=>{
                            let checkRoom=true;
                            danhSachPhongDaDat.forEach((phongDat)=>{
                                        if(item.id==phongDat.maPhong){
                                            // console.log(phongDat);
                                            // console.log(checkIn,checkOut);
                                            if( 
                                                (moment(checkIn).format('YYYY-MM-DD')<moment(phongDat.ngayDen).format('YYYY-MM-DD') && moment(checkOut).format('YYYY-MM-DD')>moment(phongDat.ngayDen).format('YYYY-MM-DD'))
                                               || (moment(checkIn).format('YYYY-MM-DD')>=moment(phongDat.ngayDen).format('YYYY-MM-DD') && moment(checkOut).format('YYYY-MM-DD')<=moment(phongDat.ngayDi).format('YYYY-MM-DD')) 
                                               || (moment(checkIn).format('YYYY-MM-DD')<moment(phongDat.ngayDi).format('YYYY-MM-DD') && moment(checkOut).format('YYYY-MM-DD')>moment(phongDat.ngayDi).format('YYYY-MM-DD'))   
                                               ){
                                                console.log(phongDat);
                                                checkRoom=false;
                                            }
                                        }
                                    })
                           return checkRoom;
                        })))
                     
                    })
                }
                placesService.getPlace(parseInt(id)).then((res)=>{
                    let {tenViTri,tinhThanh,quocGia}=res.data.content;
                    setViTri(tenViTri+","+tinhThanh+","+quocGia)
                    console.log('test',res.data.content);});
            }
        ).catch((err)=>{
            console.log(err);
        })
    },[id,checkIn,checkOut]);
   
    console.log("listRoom",listRoom);
   
  return (
    <div >
        <div className=''>
            <div className=' m-2 container mx-auto  '>
                <div className='w-full p-2 space-y-2'>
                    <span className=' text-sm text-slate-400'>{viTri}</span>
                    <h1 className=' font-bold'>Danh sách các phòng tại {viTri} </h1>
                     {checkIn=="" && checkOut==""?<></>:<i>từ {moment(checkIn).format("DD-MM")} tới {moment(checkOut).format("DD-MM")} </i>}
                    <div className='flex [&>*]:border-2 [&>*]:rounded-xl  [&>*]:p-1 space-x-2'>
                        {/* <button className=' '>loai noi o</button>
                        <button>gia</button>
                        <button>dat ngay</button>
                        <button>phong va phong ngu</button> */}
                    </div>
                </div>
                <hr />
                <div className='p-2 grid grid-cols-2 mobile:grid-cols-1'>
                    {listRoom.length==0?
                         <>
                            <h1>Hiện tại không có danh sách phòng cho địa điểm này!!!</h1>
                           </>:
                       listRoom.map((room,index)=>{
                        return <div key={index} onClick={()=>{navigate(`/detailroom/${room.id}`)}} className=' border-2 rounded-2xl p-2 m-2'>
                            <div className='mb-2 h-28'>
                                <img className='w-full h-full object-contain rounded-xl' src={room.hinhAnh} alt="" />
                            </div>
                            <div className=''>
                                <div><h3 className=' font-bold'>{room.tenPhong}</h3></div>
                                <div className=''><p>{room.moTa?.substring(1,100)+"..."}</p></div>
                                <div className='border-2 rounded-xl w-max p-2 hover:border-4 cursor-pointer'><b>{room.giaTien}$</b>/tháng</div>
                            </div>
                        </div>
                    })}
                </div>
            </div>
            {/* <div className='w-1/2'>googlemap</div> */}
        </div>
      <Footer></Footer>
    </div>
  )
}

export default ListRooms
