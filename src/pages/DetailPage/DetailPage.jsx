import React, { useState } from 'react'
import { useEffect } from 'react';
import {  useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import Footer from '../../components/Footer/Footer';
import { setRoomChoose } from '../../redux/SearchSlice';
import { placesService } from '../../services/place.service';
import Booking from './Booking';
import ListComment from './Comment/ListComment';
import PopUpBooking from './PopUpBooking/PopUpBooking';

function DetailPage()  {
    let dispatch=useDispatch();
    let viTri=useSelector((state)=>{
        return state.SearchSlice.place.viTri;
    })
    // let [roomChoose,setRoomChoose]=useState({})'
  let  roomChoose=useSelector((state)=>{
    return state.SearchSlice.roomChoose;
  })
     let {id}=useParams();
     console.log(roomChoose);
     let {banLa,banUi,bep,dieuHoa,doXe,giaTien,giuong,hinhAnh,hoBoi,khach,maViTri,mayGiat,moTa,phongNgu,phongTam,tenPhong,tivi,wifi}=roomChoose;    
useEffect(()=>{
    window.scrollTo(0,0)
        placesService.getPhongThue(id).then(
            (res)=>{
                console.log(res);
                dispatch(setRoomChoose(res.data.content))
            }
        ).catch((err)=>
         {
            console.log(err);
         })
     },[])
  return (
   <>
        
   <div>
     <div className=' p-1 container  mx-auto '>
    
        <hr />
        <div className='my-2'>
            <h1 className=' font-bold'>{tenPhong}</h1>
            <p className=' text-sm'>{viTri}</p>
        </div>
        <div className='mb-8 mt-8 mobile:mt-0 mobile:mb-0' style={{height:`30vh`}}>
            <img className='w-full h-full rounded-xl object-contain' src={hinhAnh} alt="" />
        </div>
        <div className=' flex p-1 mobile:block'>
            <div className='w-2/3 p-1 mobile:w-full '>
            <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h1>
            <i>{`${giuong} giường, ${khach} khách, ${phongNgu} phòng ngủ, ${phongTam} phòng tắm`}</i>
            <hr />
            <div id='moTa' className='my-2 '>
                {moTa}
            </div>
            <div id='tienNghi' className='p-1'>
                 <h1 className=' font-bold my-2'>Tiện nghi</h1>
                 <div className='grid grid-cols-2'>
               
                        <div><i className="fa fa-utensil-spoon"></i> Bếp:{bep?"yes":"no"}</div>
                        <div>bàn ủi:{banLa?"yes":"no"}</div>
                        <div><i className="fa-solid fa-wind"></i> điều hoà:{dieuHoa?"yes":"no"}</div>
                        <div><i className="fa-solid fa-square-parking"></i> đỗ xe:{doXe?"yes":"no"}</div>
                        <div><i className="fa-solid fa-square-parking"></i> hồ bơi:{hoBoi?"yes":"no"}</div>
                        <div><i className="fa-solid fa-tv"></i> máy giặt:{mayGiat?"yes":"no"}</div>
                        <div><i className="fa-solid fa-tv"></i> tivi:{tivi?"yes":"no"}</div>
                        <div><i className="fa-solid fa-plane-up"></i> wifi:{wifi?"yes":"no"}</div>
                 </div>
            </div>
            </div>
            <div className='w-1/3 mobile:w-full mobile:mt-5'>
                <Booking dataPrice={giaTien}></Booking>
            </div>
        </div>
        <div>
            <ListComment id={id}></ListComment>
        </div>
    </div>
   </div></>
  )
}

export default DetailPage