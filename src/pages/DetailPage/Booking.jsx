import moment from 'moment';
import React, { useState } from 'react'
import { useEffect } from 'react';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { setCheckIn, setCheckOut, setGuest } from '../../redux/SearchSlice';
import { setIsPopUpBooking, setIsPopUpLogin } from '../../redux/UserSlice';
import { localService } from '../../services/local.service';
import { placesService } from '../../services/place.service';
import PopUpBooking from './PopUpBooking/PopUpBooking';
import DatePicker from "react-datepicker";

function Booking({dataPrice}) {
    let {id}=useParams();
    let dispatch=useDispatch();
    let [listRooms,setListRooms]=useState([])
    const checkIn=useSelector((state)=>{
        return state.SearchSlice.checkIn;
    })
    const CheckOut=useSelector((state)=>{
        return state.SearchSlice.checkOut;
    })
    const guest=useSelector((state)=>{
        return state.SearchSlice.guest;
    })
     
   useEffect(()=>{
        // dispatch(setPlaceChoose(obj))
        placesService.getDatPhong().then(
             (res)=>{
                console.log(res.data.content);
           setListRooms(res.data.content);
            }
        )
    },[checkIn , CheckOut])
    let checkRoom=useCallback(()=>{
      console.log("check time",checkIn,CheckOut);
      let check=true;
     listRooms.forEach((item)=>{
          if(item.maPhong==id){
       if( 
     (moment(checkIn).format('YYYY-MM-DD')<=moment(item.ngayDen).format('YYYY-MM-DD') && moment(CheckOut).format('YYYY-MM-DD')>moment(item.ngayDen).format('YYYY-MM-DD'))
    || (moment(checkIn).format('YYYY-MM-DD')>=moment(item.ngayDen).format('YYYY-MM-DD') && moment(CheckOut).format('YYYY-MM-DD')<=moment(item.ngayDi).format('YYYY-MM-DD')) 
    || (moment(checkIn).format('YYYY-MM-DD')<moment(item.ngayDi).format('YYYY-MM-DD') && moment(CheckOut).format('YYYY-MM-DD')>moment(item.ngayDi).format('YYYY-MM-DD'))   
    ){
      console.log("come: ",item.ngayDen," go: ",item.ngayDi); 
      
       check=false;
     console.log(item);
     }      
          }
     })
     return check;
  },[checkIn,CheckOut])
    let booking=useCallback(()=>{
      //   let params={
      //    maPhong:parseInt(id),
      //    ngayDen:checkIn,
      //    ngayDi:CheckOut,
      //    soLuongKhach:parseInt(guest),
      //    maNguoiDung: localService.get().user.id,
      //   }
      if(guest<0 || guest==""){document.getElementById("slKhach").innerHTML="vui lòng nhập sl khách lớn hơn 0"}else{document.getElementById("slKhach").innerHTML=""}
      if(checkIn=="" ){document.getElementById("checkIn").innerHTML="chọn ngày"}else{document.getElementById("checkIn").innerHTML=""}
      if( CheckOut==""  ){document.getElementById("checkOut").innerHTML="chọn ngày"}else{document.getElementById("checkOut").innerHTML=""}
       if(localService.get()!=null){
        if (checkIn!='' && CheckOut!=''&& guest>0 && checkRoom()){
          dispatch(setIsPopUpBooking(true))
         }else{
           console.log('chua du dieu kien');
         }
       }else{
        dispatch(setIsPopUpLogin(true));
       }
     
     //    console.log( localService.get().user.id);
     },[checkIn,CheckOut,guest])
  return (
   <>
    <div className='border-2 shadow-lg w-full h-max p-4 rounded-lg'>
    <div className='flex justify-between p-2'>
    <b>{dataPrice}$/đêm</b>
      <span>đánh giá 10 ngôi sao</span>
    </div>
    <hr /> 
    <div className='border-2 rounded-lg my-2'>
      <div className='flex'>
       <div className='w-1/2 border-r-2 border-b-2 p-1'><p className=' text-sm text-gray-400'>nhận phòng</p>
       <DatePicker className='w-full' placeholderText='Chọn ngày đến' dateFormat='dd-MM-yyyy'  
       selected={checkIn} 
       onChange={(e)=>{ 
        dispatch(setCheckIn(e))
      }}
        />
       {/* <input value={checkIn} onChange={(e)=>{dispatch(setCheckIn(e.target.value)) }} type="date"  /> */}
       <span id='checkIn' className=' text-xs text-red-400'></span>
       </div>
       <div className='w-1/2 border-l-2 border-b-2 p-1'><p className=' text-sm text-gray-400'>trả phòng</p> 
     
       <DatePicker className='w-full' placeholderText='Chọn ngày đi' dateFormat='dd-MM-yyyy' selected={CheckOut} minDate={checkIn} onChange={(e)=>{dispatch(setCheckOut(e))}} />
       <span id='checkOut' className=' text-xs text-red-400'></span>
       {/* <input  value={CheckOut} min={checkIn} onChange={(e)=>{dispatch(setCheckOut(e.target.value))}}  type="date" /> */}
       </div>
      </div>
      <div className='p-1'><p className=' text-sm text-gray-400'>khách</p>
      <input value={guest} onChange={(e)=>{dispatch(setGuest(e.target.value))}} className='w-full' type="number" />
      <span id='slKhach' className=' text-xs text-red-400'></span>
      </div>
    </div>
    <div className=' text-center font-bold p-2'>{checkRoom()?"còn phòng":"không còn phòng"}</div>
    <button onClick={()=>{booking()}} className='w-full bg-red-300 py-3 rounded-lg'>Đặt phòng</button>
   </div>
   </>
  )
}

export default Booking