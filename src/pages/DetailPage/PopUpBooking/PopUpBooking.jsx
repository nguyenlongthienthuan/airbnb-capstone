import moment from 'moment';
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router';
import styled,{ keyframes } from 'styled-components';
import { setIsPopUpBooking } from '../../../redux/UserSlice';
import { localService } from '../../../services/local.service';
import { placesService } from '../../../services/place.service';
const animation=keyframes`
from{
 top:0;
}to{
  top:0;
}
`
const PopUpStyled=styled.div`
    #form{
      animation:${animation} 0.3s ease-in-out forwards;
    }
`
let useClickOutSide=(handler)=>{
  let domNode=useRef();
  useEffect(()=>{
   let maybeHandler=(e)=>{
        // console.log(e);
       if(!domNode.current.contains(e.target)){
           handler();
       }
   };
   document.addEventListener("mousedown",maybeHandler);
   return()=>{
       document.removeEventListener("mousedown",maybeHandler);
   };
 },[])
  return domNode
 }
function PopUpBooking() {
  let navitgate=useNavigate();
  let dispatch=useDispatch();
  const isOpenBooking=useSelector((state)=>{
    return state.UserSlice.isPopUpBooking;
})
let data=useSelector((state)=>{ 
  return state.SearchSlice;
})
let [checkBooking,setcheckBooking]=useState(true)
let {checkIn,checkOut,guest,roomChoose}=data;
// console.log(roomChoose);
let {id,banLa,banUi,bep,dieuHoa,doXe,giaTien,giuong,hinhAnh,hoBoi,khach,maViTri,mayGiat,moTa,phongNgu,phongTam,tenPhong,tivi,wifi}=roomChoose;    

// console.log(roomChoose);
const domNode=useClickOutSide(
  ()=>{
    dispatch(setIsPopUpBooking(false))
   setcheckBooking(true)
  }
)
let booking=()=>{
  let params={
   maPhong:id,
   ngayDen:moment(checkIn).format("YYYY-MM-DD"),
   ngayDi:moment(checkOut).format("YYYY-MM-DD"),
   soLuongKhach:parseInt(guest),
   maNguoiDung: localService.get().user.id,
  }
  console.log(id);
 if (checkIn!='' && checkOut!=''&& guest>0 ){
   placesService.postDatPhong(params).then((res)=>{console.log(res);setcheckBooking(false);setTimeout(() => {
       navitgate("/");
     dispatch(setIsPopUpBooking(false))
      // window.location.reload()
   }, 3000);})
 }else{
   console.log('chua du dieu kien');
 }
dispatch(setIsPopUpBooking(true))
//    console.log( localService.get().user.id);
}
  return (
    isOpenBooking? 
    <PopUpStyled>
    <div className=' fixed w-screen h-screen z-50 p-2 flex items-center' style={{
        background:`rgba(0,0,0,0.3)`,
    }}>
   
   <div ref={domNode} id="form" className=' w-4/5 max-h-max mobile:w-full tablet:w-5/6 mx-auto  flex items-center flex-col relative top-1/2 ' action="">
        {checkBooking?
         <div className=' bg-white shadow-2xl rounded p-4 flex w-full items-center  mobile:block'>
           <div id='place' className='w-1/2 mobile:w-full mobile:mx-auto  mobile:mt-5 '>
              <div className='border-2 rounded-xl p-3'>
                  <div className=' space-y-3'>
                    <img className='w-full' src={hinhAnh} alt="" /> 
                    <hr />
                    <h5 className=' font-bold'>{tenPhong}</h5>
                    <hr />
                    <p>Giá:<b>{giaTien}$</b>/night</p>
                    <p>phí dịch vụ: 0</p>
                    
                    <hr />
                    <p>TỔNG:....</p>
                  </div>
              </div>
         </div>
         <div className='w-1/2 mobile:w-full'>
         <div className='space-y-5 mx-auto w-5/6 mobile:w-full'>
         <h1 className=' font-bold text-xl'>Xác nhận và thanh toán • Airbnb</h1>
        <div className=' mobile:flex justify-between'>
        <div className=' mobile:w-2/3'>
           <h3>Chuyến đi của bạn</h3>  
              <p className=''><b>từ ngày:</b> <i>{moment(checkIn).format("DD-MM-YYYY")}</i></p> 
              <p><b>đến ngày:</b> <i>{moment(checkOut).format("DD-MM-YYYY")}</i></p> 
               <p><b>Số khách:</b> <i>{guest}</i></p>
         </div>
         <div className=' mobile:w-1/3'><button onClick={()=>{booking()}} type="submit" className=' rounded-md bg-red-200 py-2 px-3 mobile'>Xác Nhận Booking</button></div>
        </div>
         </div>
         </div>
        
        </div>:
        <>
             <div className='bg-white shadow-2xl rounded flex justify-center items-center w-full h-full'>
              <h1>Bạn đã đặt phòng thành công!!!</h1>
             </div>
        </>}
   </div>
   
</div>
</PopUpStyled>:<div ref={domNode}></div>
  )
}

export default PopUpBooking