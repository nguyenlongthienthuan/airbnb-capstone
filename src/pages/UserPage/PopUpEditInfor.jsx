import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { setIsPopUpLogin, setUserInfor } from '../../redux/UserSlice';
import { localService } from '../../services/local.service';
import { userService } from '../../services/user.service';

function PopUpEditInfor({userInfor}) {
    let dispatch=useDispatch();
    let nameRef=useRef();
    let emailRef=useRef();
    let birthdayRef=useRef();
    let phoneRef=useRef();
    let genderRef=useRef();
    let navigate=useNavigate();
    let [name,setName]=useState(userInfor.name);
    // let [mk,setMk]=useState("");
    let [email,setEmail]=useState("");
    let [phone,setPhone]=useState("");
    let [birthday,setBirthday]=useState("");
    let [gioiTinh,setGioiTinh]=useState(null);
    useEffect(()=>{
        setName(userInfor.name)
        setBirthday(userInfor.birthday)
        setEmail(userInfor.email)
        setGioiTinh(userInfor.gender)
        setPhone(userInfor.phone)
    },[userInfor])
    let validation=(id,value)=>{
        if (id=='name'){
            var regex =/^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]*$/;
                    if(value==""){
                        document.getElementById(id).innerText =
                        "không được để trống";
                        setName(value);
                        return false;
                    }else{
                        if (regex.test(value)) {
                            document.getElementById(id).innerText = "";
                            setName(value);
                            console.log(name);
                            return true;
                        } else {
                            setName(value);
                            document.getElementById(id).innerText =
                            "không nhập số và kí tự đặc biệt";
                            return false;
                        }
                    }

        }
        else if(id=='email'){
            let regex=/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
            if(value==""){
                setEmail(value)
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                if (regex.test(value)) {
                    setEmail(value)
                    document.getElementById(id).innerText = "";
                    return true;
                } else {
                    setEmail(value)
                    document.getElementById(id).innerText =
                    "không đúng định dạng email";
                    return false;
                }
            }

        }
        // else if(id=='password'){
        //     let regex=/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        //     if(value==""){
        //         document.getElementById(id).innerText =
        //         "không được để trống";
        //         return false;
        //     }else{
        //         if (regex.test(value)) {
        //             document.getElementById(id).innerText = "";
        //             setMk(value)
        //             return true;
        //         } else {
        //             document.getElementById(id).innerText =
        //             "nhập mk trên 8 kí tự";
        //             return false;
        //         }
        //     }
        // }
        else if(id=='phone'){
            let regex=/^[0-9\-\+]{9,10}$/;
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                setPhone(value)
                return false;
            }else{
                if (regex.test(value)) {
                    document.getElementById(id).innerText = "";
                    setPhone(value)
                    return true;
                } else {
                    document.getElementById(id).innerText =
                    "nhập sdt 10 số";
                    setPhone(value)
                    return false;
                }
            }
        }
        else if(id=='birthday'){
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                setBirthday(value)
                return false;
            }else{
                    setBirthday(value)
                    document.getElementById(id).innerText = "";
                    return true;
            }
        }
        else if(id=='gender'){
            if(value=="giới tính"){
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                    setGioiTinh(value)
                    document.getElementById(id).innerText = "";
                    return true;
            }
        }
    }
    let edit=useCallback(()=>{
        console.log(name);
        validation("name",name) 
        validation("email",email) 
        // validation("password",mk) 
        validation("phone",phone) 
        validation("birthday",birthday) 
        validation("gender",gioiTinh) 
        if 
      (  validation("name",name) &&
         validation("email",email) &&
        //  validation("password",mk) &&
         validation("phone",phone) &&
         validation("birthday",birthday) &&
         validation("gender",gioiTinh) ){
            let params={
                // id: 0,
                name,
                email,
                // password: mk,
                phone: `${phone}`,
                birthday: birthday,
                gender: gioiTinh,
                role: "admin",
              }
            console.log(params);
            userService.putUser(userInfor.id,params).then(
                (res)=>{
                     window.location.reload();
                     localService.delete('USER_INFOR');
                    // console.log(res);
                }
            ).catch((err)=>{console.log(err);alert(err.response.data.content)})
         }else{alert("vui lòng nhập đủ và đúng giá trị")}
    },[email,name,phone,birthday,gioiTinh])
  return (
    <div className=' w-full text-center'>
          <div className='border-2 p-2 rounded-xl space-y-4'>
       <form action="" className='flex flex-col space-y-2'>
            <label htmlFor="">Họ và tên: <input  value={name} onChange={(e)=>{validation(e.target.name,e.target.value)}} className=' border-2' name='name' type="text" placeholder='full name'/>
            </label>
            <span className=' text-xs text-red-300' id='name'></span>
             <label htmlFor="">Email:<input value={email} onChange={(e)=>{validation(e.target.name,e.target.value)}} name='email' type="text" placeholder='email-tài khoản' /></label>
             <span className=' text-xs text-red-300' id='email'></span>
             <label htmlFor="">Phone: <input value={phone} onChange={(e)=>{validation(e.target.name,e.target.value)}} name='phone' type="text" placeholder='số điện thoại' /></label>
             <span className=' text-xs text-red-300' id='phone'></span>
             <label htmlFor="">Birthday: <input value={birthday} onChange={(e)=>{validation(e.target.name,e.target.value)}} name='birthday' type="text" placeholder='ngày sinh' /></label>
             <span className=' text-xs text-red-300' id='birthday'></span>
             <label htmlFor="">Giới tính: <select onChange={(e)=>{validation(e.target.name,e.target.value)}} name="gender" >
                <option >giới tính</option>
                <option  selected={userInfor.gender?"selected":""} value={true}>nam</option>
                <option selected={userInfor.gender?"":"selected"} value={false}>nữ</option>
             </select></label>
             <span className=' text-xs text-red-300' id='gender'></span>
        </form>
        <button onClick={()=>{edit()}} className=' py-1 px-2 bg-blue-400 rounded-md hover:border-2 border-blue-400'>Edit</button>
       </div>
    </div>
  )
}

export default PopUpEditInfor