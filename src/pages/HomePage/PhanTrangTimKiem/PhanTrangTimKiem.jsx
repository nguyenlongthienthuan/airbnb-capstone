import React, { useEffect } from 'react'
import { useState } from 'react'
import { placesService } from '../../../services/place.service'

function PhanTrangTimKiem() {
    const [places, setPlaces] = useState([])
    useEffect(()=>{
         const params={
            pageIndex:1,
            pageSize:10,
         }
        placesService.getPhanTrangTimKiem({params}).then(
            (res)=>{
                // console.log(res.data.content.data);
                setPlaces(res.data.content.data.map((item)=>{
                    let {tinhThanh,hinhAnh}=item
                    return {tinhThanh,hinhAnh}}))
            }
        )   
    },[])
  return (
    <div className='p-4 container mx-auto'>
       <div className='w-full overflow-y-scroll'>
       <h1 className=' text-base font-medium'>khám phá những điểm đến gần đây</h1>
        <div className=' grid grid-cols-5 mobile:grid-cols-2 p-2 w-full ' style={{height:`20vh`}}>
                {places.map((item,index)=>{
                    return <div key={index} className=' flex items-start space-x-2'>
                            <div className='w-10 h-10'><img className='w-full rounded-md object-contain' src={item.hinhAnh} alt="" /></div>
                            <div><b className=' text-xs'>{item.tinhThanh}</b></div>
                    </div>
                })}
        </div>
       </div>
    </div>
  )
}

export default PhanTrangTimKiem