import React from 'react'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import About from './About/About'
import Banner from './Banner/Banner'
import PhanTrangTimKiem from './PhanTrangTimKiem/PhanTrangTimKiem'

function HomePage() {
  return (
    <div className=''>
        {/* <Header></Header> */}
        <Banner></Banner>
        <PhanTrangTimKiem></PhanTrangTimKiem>
        <About></About> 
        <Footer></Footer>
    </div>
  )
}

export default HomePage