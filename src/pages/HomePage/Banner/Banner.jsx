import React from 'react'

function Banner() {
  return (
    <div className='w-full mobile:h-max desktop:h-60vh' >
        <img className='w-full h-full object-contain' src='https://lh6.googleusercontent.com/pjWGe2RBIhrkFlqjerrKKTxOTzdaOMkbuwTtw93FxwtTbfkuBoI50EiH7_d-e1c5GEsXDJwMDsztNqXnNug-8rSkMpt295wDT62fJDEBWVzmFeGxjGcnHHBVGRkpD1rTXprEtoiU=s0' alt="" />
    </div>
  )
}

export default Banner