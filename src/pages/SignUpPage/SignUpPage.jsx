import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { setIsPopUpLogin } from '../../redux/UserSlice';
import { userService } from '../../services/user.service';

function SignUpPage() {
    let dispatch=useDispatch();
    let navigate=useNavigate();
    let [name,setName]=useState("");
    let [mk,setMk]=useState("");
    let [email,setEmail]=useState("");
    let [phone,setPhone]=useState("");
    let [birthday,setBirthday]=useState("");
    let [gioiTinh,setGioiTinh]=useState(null);
    let [check,setCheck]=useState(false);
    let validation=(id,value)=>{
        if (id=='name'){
            var regex =/^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]*$/;
                    if(value==""){
                        document.getElementById(id).innerText =
                        "không được để trống";
                        return false;
                    }else{
                        if (regex.test(value)) {
                            document.getElementById(id).innerText = "";
                            setName(value);
                            console.log(name);
                            return true;
                        } else {
                            document.getElementById(id).innerText =
                            "không nhập số và kí tự đặc biệt";
                            return false;
                        }
                    }

        }
        else if(id=='email'){
            let regex=/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                if (regex.test(value)) {
                    setEmail(value)
                    document.getElementById(id).innerText = "";
                    return true;
                } else {
                    document.getElementById(id).innerText =
                    "không đúng định dạng email";
                    return false;
                }
            }

        }
        else if(id=='password'){
            let regex=/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                if (regex.test(value)) {
                    document.getElementById(id).innerText = "";
                    setMk(value)
                    return true;
                } else {
                    document.getElementById(id).innerText =
                    "nhập mk trên 8 kí tự";
                    return false;
                }
            }
        }
        else if(id=='phone'){
            let regex=/^[0-9\-\+]{9,10}$/;
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                if (regex.test(value)) {
                    document.getElementById(id).innerText = "";
                    setPhone(value)
                    return true;
                } else {
                    document.getElementById(id).innerText =
                    "nhập sdt 10 số";
                    return false;
                }
            }
        }
        else if(id=='birthday'){
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                    setBirthday(value)
                    document.getElementById(id).innerText = "";
                    return true;
            }
        }
        else if(id=='gender'){
            if(value==""){
                document.getElementById(id).innerText =
                "không được để trống";
                return false;
            }else{
                    setGioiTinh(value)
                    document.getElementById(id).innerText = "";
                    return true;
            }
        }
    }

    


    let signUp=useCallback(()=>{
        console.log(name);
        validation("name",name) 
        validation("email",email) 
        validation("password",mk) 
        validation("phone",phone) 
        validation("birthday",birthday) 
        validation("gender",gioiTinh) 
        if 
      (  validation("name",name) &&
         validation("email",email) &&
         validation("password",mk) &&
         validation("phone",phone) &&
         validation("birthday",birthday) &&
         validation("gender",gioiTinh) ){
            let params={
                // id: 0,
                name,
                email,
                password: mk,
                phone: `${phone}`,
                birthday: birthday,
                gender: gioiTinh,
                role: "string",
              }
            console.log(params);
            userService.postSignUp(params).then(
                (res)=>{
                     navigate("/")
                     dispatch(setIsPopUpLogin(true));
                    // console.log(res);
                }
            ).catch((err)=>{console.log(err);alert(err.response.data.content)})
         }else{alert("vui lòng nhập đủ và đúng giá trị")}
    },[email,name,mk,phone,birthday,gioiTinh])
  return (
    <div className=' w-screen flex flex-col items-center h-screen justify-start bg-slate-100'>
       <div className='border-2 p-2 rounded-xl space-y-4'>
       <form action="" className='flex flex-col space-y-2'>
            
            <label htmlFor="">Họ và tên: <input onChange={(e)=>{validation(e.target.name,e.target.value)}} className=' border-2' name='name' type="text" placeholder='full name'/>
            </label>
            <span className=' text-xs text-red-300' id='name'></span>
             <label htmlFor="">Email:<input onChange={(e)=>{validation(e.target.name,e.target.value)}} name='email' type="text" placeholder='email-tài khoản' /></label>
             <span className=' text-xs text-red-300' id='email'></span>
             <label htmlFor="">PassWord: <input onChange={(e)=>{validation(e.target.name,e.target.value)}} name='password' type="text" placeholder='mật khẩu' /></label>
             <span className=' text-xs text-red-300' id='password'></span>
             <label htmlFor="">Phone: <input onChange={(e)=>{validation(e.target.name,e.target.value)}} name='phone' type="text" placeholder='số điện thoại' /></label>
             <span className=' text-xs text-red-300' id='phone'></span>
             <label htmlFor="">Birthday: <input onChange={(e)=>{validation(e.target.name,e.target.value)}} name='birthday' type="text" placeholder='ngày sinh' /></label>
             <span className=' text-xs text-red-300' id='birthday'></span>
             <label htmlFor="">Giới tính: <select onChange={(e)=>{validation(e.target.name,e.target.value)}} name="gender" >
                <option value="nam">giới tính</option>
                <option value={true}>nam</option>
                <option value={false}>nữ</option>
             </select></label>
             <span className=' text-xs text-red-300' id='gender'></span>
        </form>
        <div className=' space-x-3'>
        <button onClick={()=>{signUp()}} className=' py-1 px-2 bg-blue-400 rounded-md hover:border-2 border-blue-400'>Sign up</button>
        <button onClick={()=>{dispatch(setIsPopUpLogin(true)); navigate("/")}}>Sign In</button>
        </div>
       </div>
    </div>
  )
}

export default SignUpPage