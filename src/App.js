import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import ListRooms from './pages/ListRooms/ListRooms';
import Header from './components/Header/Header';
import PopUpLogin from './components/popUpLogin/PopUpLogin';
import DetailPage from './pages/DetailPage/DetailPage';
import PopUpBooking from './pages/DetailPage/PopUpBooking/PopUpBooking';
import SignUpPage from './pages/SignUpPage/SignUpPage';
import UserPage from './pages/UserPage/UserPage';
import "react-datepicker/dist/react-datepicker.css";
function App() {
  return (
    <div className=' relative'>
     <BrowserRouter>
     <PopUpLogin></PopUpLogin>
     <PopUpBooking></PopUpBooking>
     <Header></Header>
     <Routes>
      <Route path='/' element={<HomePage></HomePage>}>
      </Route>
      <Route path='/danhsachphong/:id' element={<ListRooms></ListRooms>}></Route>
      <Route path='/detailroom/:id' element={<DetailPage></DetailPage>}>
      </Route>
      <Route path='/signup' element={<SignUpPage></SignUpPage>}>
      </Route>
      <Route path='/user/:id' element={<UserPage></UserPage>}>
      </Route>
     </Routes>
     </BrowserRouter>
    </div>
  );
}

export default App;
