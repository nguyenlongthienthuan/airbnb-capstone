import { localService } from "./local.service";

import axios from "axios"
let baseURL="https://airbnbnew.cybersoft.edu.vn";
let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";
let configHeader=()=>{
    return {
        TokenCybersoft: token,
        Authorization:"Bearer "+localService.get()?.token,
    }
}
export const http=axios.create({
    baseURL,
    headers:configHeader(),
})