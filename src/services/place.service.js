import { http } from "./url.config.service";

export const placesService={
    getListPlaces:()=>{
        let uri="/api/vi-tri";
        return http.get(uri);
    },
    getPlace:(id)=>{
        let uri=`/api/vi-tri/${id}`;
        return http.get(uri);
    },
    getPhanTrangTimKiem:(params)=>{
        let uri="/api/vi-tri/phan-trang-tim-kiem";
        return http.get(uri,params);
    },
    getPhongTheoViTri:(params)=>{
        let uri="/api/phong-thue/lay-phong-theo-vi-tri";
        return http.get(uri,{params})
    },
    getPhongThue:(id)=>{
        // console.log(id);
        let uri=`/api/phong-thue/${id}`
        return http.get(uri);
    },
    getDatPhong:()=>{
        let uri='/api/dat-phong';
        return http.get(uri);
    },
    postDatPhong:(params)=>{
        console.log(params);
        let uri='/api/dat-phong';
        return http.post(uri,params)
    },
    getCommentRoom:(id)=>{
        let uri=`/api/binh-luan/lay-binh-luan-theo-phong/${id}`;
        return http.get(uri)
    }
}