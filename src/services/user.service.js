import { http } from "./url.config.service";

export const userService={
    postLogin:(params)=>{
        console.log(params);
        let uri="/api/auth/signin";
      return  http.post(uri,params)
    },
    postSignUp:(params)=>{
       let uri="/api/auth/signup";
       return http.post(uri,params)
    },
    getInforUser:(id)=>{
      console.log(id);
      let uri=`/api/users/${id}`;
      return http.get(uri)
    },
    getDatPhongUser:(MaNguoiDung)=>{
      let uri=`/api/dat-phong/lay-theo-nguoi-dung/${MaNguoiDung}`;
      return http.get(uri)
    },
    putUser:(id,params)=>{
      let uri=`/api/users/${id}`;
      return http.put(uri,params);
    }
    
}