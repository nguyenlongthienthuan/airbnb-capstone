import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router';
import styled, { keyframes } from 'styled-components';
import { setIsPopUpLogin, setUserInfor } from '../../redux/UserSlice';
import { localService } from '../../services/local.service';
import { userService } from '../../services/user.service';

const animation=keyframes`
from{
 top:100%;
}to{
  top:50%;
}
`
const PopUpStyled=styled.div`
    #form{
      animation:${animation} 0.3s ease-in-out forwards;
    }
`
let useClickOutSide=(handler)=>{
  let domNode=useRef();
  useEffect(()=>{
   let maybeHandler=(e)=>{
        // console.log(e);
       if(!domNode.current.contains(e.target)){
           handler();
       }
   };
   document.addEventListener("mousedown",maybeHandler);
   return()=>{
       document.removeEventListener("mousedown",maybeHandler);
   };
 },[])
  return domNode
 }
// let IsClickOutSide=(handler)=>{
//       let domNode=useRef(); 
//       useEffect(()=>{
//        let maybeHandler=(e)=>{
//         if(!domNode.current.contains(e.target)){
//           handler();
//         }
//        }
//        document.addEventListener("mousedown",maybeHandler);
//        return()=>{
//         document.removeEventListener("mousedown",maybeHandler);
//        }
//       },[])
//       return domNode;
// }
function PopUpLogin() {
    let navigate=useNavigate()
    let dispatch=useDispatch()
    let passWordRef=useRef();
    let userNameRef=useRef();
    const isOpenLogin=useSelector((state)=>{
        return state.UserSlice.isPopUpLogin;
    })
    let login=()=>{
        const params={
          email: userNameRef.current.value,
          password: passWordRef.current.value,
        }
        userService.postLogin(params).then(
          (res)=>{
            console.log("thong tin user",res.data.content);
            localService.set(res.data.content)
            // navigate("/");
           dispatch( setUserInfor(res.data.content))
            dispatch(setIsPopUpLogin(false))
            window.scrollTo(0, 0);
          }
        ).catch(
          (err)=>{
            console.log(err);
            alert(err.response.data.content)
          }
        )
    } 
    const domNode=useClickOutSide(
      ()=>{
        dispatch(setIsPopUpLogin(false))
      }
    )
  return (
    isOpenLogin? 
    <PopUpStyled>
    <div className=' fixed w-screen h-screen z-50' style={{
        background:`rgba(0,0,0,0.3)`,
    }}>
   
   <div ref={domNode} id="form" className=' w-1/3 h-1/3 mobile:h-max mobile:w-2/3 tablet:w-2/3 mx-auto text-center bg-white shadow-2xl rounded flex items-center flex-col relative top-1/2 -translate-y-1/2' action="">
       <div  className='w-full relative p-3'>
        <h1>Đăng nhập hoặc đăng kí</h1>
        <div onClick={()=>{dispatch(setIsPopUpLogin())}} className=' absolute top-0 right-10'><i  class="fa fa-times"></i></div>
       </div>
       <hr className='w-full'/>
       <div className=' space-y-5 p-5'>
       <div>tài khoản: <input className=' border-2 rounded' ref={userNameRef} placeholder='email' type="text" /></div>
       <div>mật khẩu: <input className=' border-2 rounded'  ref={passWordRef} placeholder='mật khẩu' type="password" /></div>
       </div>
      <div className=' space-x-5'>
      <button className='py-3' type="submit" onClick={login}>Login</button>
       <a href="#" onClick={()=>{
        navigate("/signup");
        dispatch(setIsPopUpLogin(false));
       }}>SignUp</a>
      </div>
   </div>
   
</div>
</PopUpStyled>:<div ref={domNode}></div>
  )
}

export default PopUpLogin