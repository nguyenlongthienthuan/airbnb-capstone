import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router';
import { setIsStartSearch } from '../../../redux/SearchSlice';
import { setIsPopUpLogin } from '../../../redux/UserSlice';
import { localService } from '../../../services/local.service';
import PopupSearchMobile from './PopupSearchMobile';

function MobileHeader() {
  let navigate=useNavigate();
  let dispatch=useDispatch();
  let userInfor=useSelector((state)=>{return state.UserSlice.userInfo})
  return (
   <>
    <PopupSearchMobile></PopupSearchMobile>
    <div className=' relative w-full '>
        <div  className='p-4  bg-white'>
        <div onClick={()=>{dispatch(setIsStartSearch(1))}} className='container mx-auto shadow-xl rounded-full flex ' style={{border:`1px solid silver`}}>
       <div className='p-2 px-3 flex items-center space-x-3 text-sm'>
       <i className="fa fa-search"></i>
       <div>
        <h5>Bạn sẽ đi đâu?</h5>
        <div style={{fontSize:`10px`}}><span>Địa điểm. </span><span>tuần bất kì. </span><span>Thêm khách</span></div>
       </div>
       </div>
        </div>
        </div>
      
    </div>
    <div className=' fixed w-full max-h-max p-1 bottom-0 bg-red-50 '>
         <div className='w-full h-full flex justify-center items-center space-x-10'> 
         <button onClick={()=>{navigate("/");window.scrollTo(0, 0);}} className=' flex flex-col items-center'>
         <i className="fa fa-search"></i>
         <p>Khám Phá</p>
         </button>
         <button className=' flex flex-col items-center'>
         <i className="fa fa-heart"></i>
         <p>Yêu Thích</p>
         </button>
         {/*  */}
         {userInfor==null? 
         <button onClick={()=>{
          dispatch(setIsPopUpLogin(true))
     }} className=' flex flex-col items-center'>
     <i className="fa fa-user"></i>
     <p>Đăng nhập</p>
     </button>:
     <button onClick={()=>{navigate(`/user/${localService.get().user.id}`)}} className=' flex flex-col items-center'>
     <i className="fa fa-user"></i>
     <p>Hồ sơ</p>
     </button> 
        }
         {/*  */}
         </div>
      </div>
     </>
  )
}

export default MobileHeader