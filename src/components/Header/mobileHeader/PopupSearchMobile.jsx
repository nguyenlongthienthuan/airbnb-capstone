import moment from 'moment';
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router';
import styled, { keyframes } from 'styled-components';
import { setCheckIn, setCheckOut, setGuest, setIsStartSearch, setPlace } from '../../../redux/SearchSlice';
import { placesService } from '../../../services/place.service';
import DatePicker from "react-datepicker";

const animation=keyframes`
from{
 top:100%;
}to{
  top:0%;
}
`
const PopupSearchMobileStyled=styled.div`
  // #search.active{
  //     #editSearch{
  //       display:block;
  //     }
  //     #resultSearch{
  //       display:none;
  //     }
  // }
  #form{
    animation:${animation} 0.2s ease-in-out forwards;
  }
  // .listSearch{
     .search{
          &[data-active]{
             border:2px solid black;
            #editSearch{
              display:block;
            }
            #resultSearch{
              display:none;
            } 
          }
          :hover{
            border:1px solid white ;
          }
      
     }
  }
`

function PopupSearchMobile() {
  let navigate=useNavigate();
  let inputPlace = useRef();
  // let timeCome=useRef();
  // let timeGo=useRef();
  let slkhach=useRef();
  let dispatch = useDispatch();
  let isStartSearch = useSelector((state) => {
    return state.SearchSlice.isStartSearch;
  })
  const [places, setplaces] = useState([]);
  const search = useSelector((state) => {
    return state.SearchSlice;
  })
 
  let { place, checkIn, checkOut, guest } = search;
  // 
  let ngayDen=new Date(checkIn==""?null:checkIn);
  useEffect(() => {
    placesService.getListPlaces().then(
      (res) => {
        // console.log(res.data.content);
        setplaces((state) => {
          return res.data.content.map((item) => { return { viTri: `${item.tenViTri + "," + item.tinhThanh + "," + item.quocGia}`, maViTri: item.id } })
        })
      }
    ).catch((err) => { console.log(err); })

  }, [isStartSearch])
  
  let activeSearch=useCallback((e)=>{

      let active=e.target.closest(".listSearch").querySelector('[data-active]');
      // console.log(e.target);
      e.target.closest(".search").dataset.active=true;
     if(!e.target.closest(".inputSearch") && active){
      delete active.dataset.active;
     }
    // console.log(e.target.closest(".inputSearch"));
   
  },[isStartSearch])
  let listSearchPlaces = places.filter((item) => { return item.viTri.toLowerCase().includes(place.viTri ? place.viTri.toLowerCase() : "") });
  return (isStartSearch ? <PopupSearchMobileStyled>
    <div id='form'  className='listSearch w-screen h-screen fixed z-50 bg-neutral-200 p-3 '>
      <div className=' relative p-2 text-center space-x-3 '>
        <i onClick={()=>{dispatch(setIsStartSearch(false))}} className="fa fa-times-circle absolute top-1/2 -translate-y-1/2 left-5 text-xl"></i> <b>chỗ ở</b><b>trãi nghiệm</b>
      </div>
      <div id='search' onClick={(e)=>{activeSearch(e)}} className='search bg-white rounded-xl p-3 mt-5 active' data-active>
          <div id='editSearch' className='hidden'>
             <div><h1 className=' font-bold'>Bạn sẽ đi đâu</h1></div>
             <div  className=' border-2 rounded-xl'>
              <label id='' htmlFor="" className='inputSearch p-3 flex items-center space-x-4'>  <i className="fa fa-search"></i><input ref={inputPlace} value={place.viTri} onChange={(e) => { dispatch(setPlace({ viTri: e.target.value })) }} placeholder='tim kiem dia diem' className='w-full' type="text" /></label>
            </div>
            <div id='listPlaces' className=' overflow-y-scroll p-3' style={{ height: `30vh` }}>
              {
                listSearchPlaces.map((item,index) => {
                  return <p key={index} className=' hover:bg-gray-400 cursor-pointer' onClick={(e) => { dispatch(setPlace(item)); dispatch(setIsStartSearch(2)) }}>{item.viTri}</p>
                })
              }
               </div>
               </div>
            <div id='resultSearch' className='flex justify-between'><p>Địa điểm</p> <p>{place.viTri}</p></div>  
      </div>
      <div id='search' onClick={(e)=>{activeSearch(e); 
    }} className='search bg-white rounded-xl p-3 mt-5 active' >
          <div id='editSearch' onClick={()=>{
                            document.getElementsByClassName('react-datepicker-wrapper')[0].getElementsByTagName('input')[0].type="button";
                            // document.getElementsByClassName('react-datepicker-wrapper')[1].getElementsByTagName('input')[0].type="button"
                        
          }} className='hidden'>
             <div><h1 className=' font-bold'>Chuyến đi của bạn sẽ diễn ra khi nào?</h1></div>
             <div  className=' border-2 rounded-xl'>
              <label id='' htmlFor="" className='inputSearch p-3 flex items-center space-x-4'>  
              {/* .react-datepicker-wrapper .react-datepicker__input-container input[type=button]{} */}
              {/* <DatePicker className='w-full'  selected={checkIn==""?new Date():new Date(checkIn)} dateFormat='dd-MM-yyyy' onChange={(date)=>{ */}

              <DatePicker  className=' text-center w-full bg-transparent' dateFormat='dd-MM-yyyy' placeholderText=' ngày đến' 
              selected={checkIn} 
              onChange={(e)=>{dispatch(setCheckIn(e))}}  />
              <DatePicker  className=' text-center w-full bg-transparent' dateFormat='dd-MM-yyyy' placeholderText=' ngày đi' 
               selected={checkOut} minDate={checkIn} onChange={(e)=>{dispatch(setCheckOut(e))}} />
            
              {/* <input ref={timeCome} value={checkIn} onChange={(e)=>{dispatch(setCheckIn(moment(e.target.value).format("YYYY-MM-DD")))}}  className='form-control  w-full h-auto bg-transparent' type="date" placeholder='thêm ngày'/> */}
              {/* <input ref={timeGo} value={checkOut} min={checkIn}  onChange={(e)=>{dispatch(setCheckOut(moment(e.target.value).format("YYYY-MM-DD")))}} className='  w-full bg-transparent' type="date" placeholder='thêm ngày'/> */}
              </label>

            </div>
            
               </div>
            <div id='resultSearch' className='flex justify-between'><p>Thời gian</p> <p>{checkIn=="" || checkOut==""?<i>chọn thời gian </i>:moment(checkIn).format("DD-MM")+"->"+moment(checkOut).format("DD-MM")}</p></div>  
      </div>
      <div id='search' onClick={(e)=>{activeSearch(e);slkhach.current.focus();}} className='search bg-white rounded-xl p-3 mt-5 active' >
          <div id='editSearch' className='hidden'>
             <div><h1 className=' font-bold'>Nhập số lượng khách trong chuyến đi</h1></div>
             <div  className=' border-2 rounded-xl'>
              <label id='' htmlFor="" className='inputSearch p-3 flex items-center space-x-4'>  
              <input ref={slkhach} value={guest} onChange={(e)=>{dispatch(setGuest(e.target.value))}} className='  w-full bg-transparent' type="text" placeholder='thêm khách'/>

              </label>
               
            </div>
            
               </div>
            <div id='resultSearch' className='flex justify-between'><p>Số lượng khách: </p> <p>{guest==null?<i>nhập sl khách</i>:guest}</p></div>  
      </div>
      <div className='mt-5'>
      <button onClick={()=>{navigate(`/danhsachphong/${place.maViTri}`);dispatch(setIsStartSearch(false))}} className='py-1 px-3 bg-red-100 rounded-xl hover:border-2'>Tìm phòng</button>
      </div>
    </div>
  </PopupSearchMobileStyled> : <></>
  )
}

export default PopupSearchMobile