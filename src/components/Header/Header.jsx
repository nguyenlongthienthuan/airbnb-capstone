import React, { useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import styled, { keyframes } from 'styled-components'
import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../HOC/Reponsive'
import { setIsStartSearch } from '../../redux/SearchSlice'
import DesktopHeader from './desktopHeader/DesktopHeader'
import HeaderSearch from './desktopHeader/HeaderSearch'
import User from './desktopHeader/User'
import MobileHeader from './mobileHeader/MobileHeader'

const UserMenu=keyframes`
from{
    transform: translateY(100%);
    height: 0;
    padding:0 10px;
  }to{
    transform: translateY(103%);
    height: max-content;
    padding:10px 10px;
  }
`
const HeaderMenu=keyframes`
from{
    width: 0;
  }to{
    width: 100%;
  }
`

const HeaderStyled=styled.div`
   [data-header-menu]{
     &:hover{
        cursor: pointer;
     }
        [data-active]{
            position: relative;
            :after{
                position: absolute;
                bottom: -10px;
                left:0;
                content:"";
                animation:${HeaderMenu} 0.1s ease-in-out forwards;
                height:2px;
                background:white;  
            }
        }
    }
    #user{
        #userMenu{
           
            &.active{
                display:block;
                animation:${UserMenu} 0.1s ease-in-out forwards;
            }
        }
       
    }

`
// let useClickOutSide=(handler)=>{
//   let domNode=useRef();
//   useEffect(()=>{
//    let maybeHandler=(e)=>{
//        if(!domNode.current.contains(e.target)){
//            handler();
//        }
//    };
//    document.addEventListener("mousedown",maybeHandler);
//    return()=>{
//        document.removeEventListener("mousedown",maybeHandler);
//    };
//  })
//   return domNode
//  }
function Header() {
    let dispatch=useDispatch()
    // let domNode=useClickOutSide(()=>{
    //   dispatch(setIsStartSearch(false))
    // })
    return (
     <>
     <DesktopReponsive>
         <DesktopHeader></DesktopHeader>
     </DesktopReponsive>
     <TabletReponsive>
      <DesktopHeader></DesktopHeader>
     </TabletReponsive>
     <MobileReponsive>
      <MobileHeader></MobileHeader>
     </MobileReponsive>
     </>
    )
}

export default Header