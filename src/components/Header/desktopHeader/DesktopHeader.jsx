import React, { useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import styled, { keyframes } from 'styled-components'
import { setIsStartSearch } from '../../../redux/SearchSlice'
import HeaderSearch from './HeaderSearch'
import User from './User'

const UserMenu=keyframes`
from{
    transform: translateY(100%);
    height: 0;
    padding:0 10px;
  }to{
    transform: translateY(103%);
    height: max-content;
    padding:10px 10px;
  }
`
const HeaderMenu=keyframes`
from{
    width: 0;
  }to{
    width: 100%;
  }
`

const HeaderStyled=styled.div`
   [data-header-menu]{
     &:hover{
        cursor: pointer;
     }
        [data-active]{
            position: relative;
            :after{
                position: absolute;
                bottom: -10px;
                left:0;
                content:"";
                animation:${HeaderMenu} 0.1s ease-in-out forwards;
                height:2px;
                background:white;  
            }
        }
    }
    #user{
        #userMenu{
           
            &.active{
                display:block;
                animation:${UserMenu} 0.1s ease-in-out forwards;
            }
        }
       
    }

`
function DesktopHeader() {
    let dispatch=useDispatch()
    // let domNode=useClickOutSide(()=>{
    //   dispatch(setIsStartSearch(false))
    // })
    return (
      <HeaderStyled>
          <header  className='  bg-white w-full  bg-opacity-90 p-3 mobile:p-2 tablet:p-1'>
          <div className='flex justify-between  mx-auto container tablet:w-full mobile:w-screen mobile:p-2'>
         <NavLink to={'/'}> <div id='logo' className=' text-2xl'><i className="fab fa-airbnb"></i> 
          <span className=" tablet:hidden mobile:hidden">Airbnb</span></div></NavLink>
          <nav className=''>
                <HeaderSearch></HeaderSearch>
          </nav>
          <div>
             <div className='flex justify-center space-x-3 items-center'>
               <p >Đón tiếp Khách</p>
                <i className="fa fa-globe"></i>
               <User></User>
             </div>
          </div>
          </div>
      </header>
      </HeaderStyled>
    )
}

export default DesktopHeader