import moment from 'moment'
import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled, { keyframes } from 'styled-components'
import { setIsStartSearch } from '../../../redux/SearchSlice'
import Search from './Search/Search'

const startSearch1 = keyframes`
from {
  transform :scale(0);
   margin-top:0;
   opacity:1;
 }
 to {
  opacity:0;
   transform :scale(1);
   margin-top:80px;
   width:100%
 }
`
const startSearch2 = keyframes`
from {
  transform :scale(0);
  opacity:0;
 }
 to {
   transform :scale(1);
   transform: translateX(-50%);
   opacity:1;
 }
`
const SearchStyled = styled.div`
// width:70%;
 .startSearch1{
  animation:${startSearch1} 0.3s ease-in-out forwards;
 }
 .startSearch2{
  animation:${startSearch2} 0.7s ease-in-out forwards;
 }
`
let useClickOutSide = (handler) => {
  let domNode = useRef();
  useEffect(() => {
    let maybeHandler = (e) => {
      // console.log(e);
      if (!domNode.current.contains(e.target)) {
        handler();
      }
    };
    document.addEventListener("mousedown", maybeHandler);
    return () => {
      document.removeEventListener("mousedown", maybeHandler);
    };
  }, [])
  return domNode
}
function HeaderSearch() {
  let dispatch = useDispatch();
  let [search, setSearch] = useState(false);
  let domNode = useClickOutSide(() => {
    // dispatch(setIsStartSearch(false))
    setSearch(false)
  })
  const searchSlice=useSelector((state)=>{
    return state.SearchSlice;
})

let {place,checkIn,checkOut,guest}=searchSlice;
  return (
     <SearchStyled>
     <div ref={domNode} className={` `}>
      <div className={` w-max p-3 shadow hover:shadow-xl rounded-full bg-white text-black space-x-3 border-2 mx-auto flex justify-between ${search?"startSearch1":""}`}>
      <button className='hover:bg-slate-200 border-black rounded-3xl ' onClick={(e)=>{setSearch(true);dispatch(setIsStartSearch(e.target.dataset.index))}} data-index={1}>{place.viTri==""?'địa điểm':place.viTri}</button>
      <button className='hover:bg-slate-200 border-black rounded-3xl' onClick={(e)=>{setSearch(true);dispatch(setIsStartSearch(e.target.dataset.index))}} data-index={2}>{checkIn==checkOut?'tuần bất kì':`${moment(checkIn).format('DD/MM')}-${moment(checkOut).format('DD/MM')}`}</button>
      <button className='hover:bg-slate-200 border-black rounded-3xl' onClick={(e)=>{setSearch(true);dispatch(setIsStartSearch(e.target.dataset.index))}} data-index={3}>thêm khách</button>
      </div>
    
     {search? <div className='startSearch2 absolute top-10 left-1/2 -translate-x-1/2 container '>
          <div className='space-x-3 text-center -top-full w-full p-2 ' data-header-menu>
                  <span className=' text-sm' data-active>Nơi ở</span>
                  <span className=' text-sm'>Trải nghiệm</span>
                  <span className=' text-sm'>Trải nghiệm trực tuyến</span>
            </div>
      <div className='relative'><Search setSearch={setSearch}></Search></div>
     </div>:<></>}
     </div>
     </SearchStyled>
  )
}

export default HeaderSearch