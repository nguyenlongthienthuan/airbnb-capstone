import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { placesService } from '../../../../services/place.service'
import { setCheckIn, setCheckOut, setGuest, setIsStartSearch, setPlace } from '../../../../redux/SearchSlice'
import moment from 'moment'
import ReactDatePicker from 'react-datepicker'
import DatePicker from "react-datepicker";
const SearchStyled=styled.div`
width:100%;
color:black;
    input{
       border:1px sloid orange;
       background:white;
       position:relative;
       &:after{
        content:"";
        position: absolute;
        width:100%;
        height:auto;
        bottom:0;
        left:0;
       }
    }
    #list_places{
        p{
            cursor:pointer;
            &:hover{
                background:#aaa;
            }
        }
    }
   
`
let useClickOutSide=(handler)=>{
    let domNode=useRef();
    useEffect(()=>{
     let maybeHandler=(e)=>{
         if(!domNode.current.contains(e.target)){
             handler();
         }
     };
     document.addEventListener("mousedown",maybeHandler);
     return()=>{
         document.removeEventListener("mousedown",maybeHandler);
     };
   },[])
    return domNode
   }
function Search({setSearch}) {
   
    // console.log(dataIndex);
    const dataIndex=useSelector((state)=>{
        return state.SearchSlice.isStartSearch;
      })
    let dispatch=useDispatch();
    let inputPlace=useRef();
    let inputGuest=useRef();
    let navigate=useNavigate();
    const [places, setPlaces] = useState([]);
    const [isSearchPlace,setIsSearchPlace]=useState(false);
    // const [searchPlace,setSearchPlace]=useState({viTri:'',maViTri:null});
    const search=useSelector((state)=>{
        return state.SearchSlice;
    })
  
    let {place,checkIn,checkOut,guest}=search;
    useEffect(()=>{
        placesService.getListPlaces().then(
            (res)=>{
                // console.log(res.data.content);
                setPlaces((state)=>{
                    return res.data.content.map((item)=>{return {viTri:`${item.tenViTri+","+item.tinhThanh+","+item.quocGia}`,maViTri:item.id}})
                })
            }
        ).catch((err)=>{console.log(err);})
      
    },[])
   
    useEffect(()=>{
        if (dataIndex==1){
            // console.log('autofocus');
           inputPlace.current.focus();
        }
        else if (dataIndex==2){
            // console.log('autofocus');
    
          setTimeout(() => {
            document.getElementsByClassName('react-datepicker-wrapper')[0]?.getElementsByTagName("input")[0]?.focus();
          }, 410);
        //    inputPlace.current.focus();
        }else{
            inputGuest.current.focus();
        }
    },[dataIndex])
    
    // let searchPlaceNode=useRef()
    let searchPlaceNode=useClickOutSide(()=>{
        setIsSearchPlace(false)
    })
    let listSearchPlaces=places.filter((item)=>{ return item.viTri.toLowerCase().includes(place.viTri?place.viTri.toLowerCase():"")});
   console.log('search');
  return (
    <SearchStyled>
        <div className=' w-full rounded-full bg-white shadow-3xl border flex justify-between p-1' >
        <div ref={searchPlaceNode} className=' w-2/6  rounded-full h-max p-2 relative'><div>địa điểm</div><input ref={inputPlace} value={place.viTri} onChange={(e)=>{dispatch(setPlace({viTri:e.target.value}))}} onFocus={()=>{setIsSearchPlace(true)}} className=' w-full bg-transparent focus:border-none' type="text" placeholder='bạn đang ở đâu'/>
        {<div id="list_places" className={` bg-white border-2 absolute w-full h-60 overflow-y-scroll left-0 p-3 rounded-xl z-50 ${isSearchPlace?"block":"hidden"}`} style={{top:'110%'}}>
         {
            // places.map((item)=>{return <p onClick={(e)=>{inputPlace.current.value=e.target.innerHTML}}>{item.tenViTri},{item.tinhThanh},{item.quocGia}</p>})
            listSearchPlaces.map((item,index)=>{return <p key={index} onClick={(e)=>{dispatch(setPlace(item))}}>{item.viTri}</p>})
         } 
         </div>}
        </div>
        <form className=' w-1/6 bg-white rounded-3xl h-max p-2 overflow-hidden'><div>nhận phòng</div>
        <DatePicker autoFocus={false}   className='w-full'  selected={checkIn} dateFormat='dd-MM-yyyy' onChange={(date)=>{
            // console.log(date);
            // console.log(moment(date).format("MM DD YYYY"));
            dispatch(setCheckIn(date))
            }} />
        {/* <input ref={timeCome} value={checkIn} onChange={(e)=>{dispatch(setCheckIn(moment(e.target.value).format("YYYY-MM-DD")))}}  className='form-control  w-full h-auto bg-transparent' type="date" placeholder='thêm ngày'/> */}
        </form>
        <div className='w-1/6 bg-white rounded-3xl h-max p-2 overflow-hidden'><div>trả phòng</div>
        <DatePicker className='w-full' selected={checkOut} dateFormat='dd-MM-yyyy' minDate={checkIn} onChange={(e)=>{dispatch(setCheckOut(e))}} />
        {/* <input ref={timeGo} value={checkOut} min={checkIn}  onChange={(e)=>{dispatch(setCheckOut(moment(e.target.value).format("YYYY-MM-DD")))}} className='  w-full bg-transparent' type="date" placeholder='thêm ngày'/> */}
        </div>
        <div className=' w-2/6 bg-white rounded-3xl h-max p-2 overflow-hidden flex justify-between'>
            <div className='w-2/3'>
            <div>khách</div>
            <input ref={inputGuest} value={guest} onChange={(e)=>{dispatch(setGuest(e.target.value))}} className='  w-full bg-transparent' type="text" placeholder='thêm khách'/>
            </div>
            <div onClick={()=>{if(place.maViTri!='')
                                    {navigate(`/danhsachphong/${place.maViTri}`);setSearch(false)}
                                    else {alert("hãy  chọn vị trí điểm đến")}
                              }} className='w-10 h-10 rounded-full bg-red-100 flex justify-center items-center'>
            <i className="fa fa-search"></i>
            </div>
            </div>
    </div>
    </SearchStyled>
  )
}

export default Search