import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import styled from 'styled-components';
import { setIsPopUpLogin, setUserInfor } from '../../../redux/UserSlice';
import { localService } from '../../../services/local.service'
// import PopUpLogin from '../popUpLogin/PopUpLogin';
const UserStyled=styled.div`
 *{
  &:hover{
    background:#aaa;
    cursor:pointer;
  }
 }
`
let useClickOutSide=(handler)=>{
 let domNode=useRef();
 useEffect(()=>{
  let maybeHandler=(e)=>{
      if(!domNode.current.contains(e.target)){
          handler();
          // console.log(domNode.current);
      }
  };
  document.addEventListener("mousedown",maybeHandler);
  return()=>{
      document.removeEventListener("mousedown",maybeHandler);
  };
},[])
 return domNode
}
function User() {
//          
  let navigate=useNavigate();
  let dispatch=useDispatch();
    const [isOpen, setIsOpen] = useState(false) 
    let userInfor=useSelector((state)=>{
      // console.log(state.UserSlice.userInfor);
        return state.UserSlice.userInfo;
    });
    // let userInfor=1

let domNode=useClickOutSide(()=>{
  setIsOpen(false)
  })
  let logout=()=>{
    localService.delete('USER_INFOR');
    dispatch(setUserInfor(null));
  }
  return ( 
    <div ref={domNode}  id='user' className=' relative text-black'>
      <div className='bg-white px-5 py-3 rounded-full space-x-3 border-2' onClick={()=>{setIsOpen((isOpen)=>!isOpen)}}>
        <i className="fa fa-bars"></i><i className="fa fa-user"></i>
        </div> 
       <div  id='userMenu' className={`absolute z-50 bg-white shadow-2xl w-56 h-0 right-0 bottom-0 translate-y-full rounded-xl overflow-hidden p-0 space-y-2 ${isOpen?"active":""}`} >
         {
         userInfor?
         <UserStyled>
         <p onClick={()=>{navigate(`/user/${localService.get().user.id}`)}}>{userInfor.user.name}</p>
         <p onClick={logout}>đăng xuất</p>
         </UserStyled>:
         <UserStyled>
         <p onClick={()=>{navigate("/signup")}}>Đăng kí</p> 
         <p onClick={()=>{dispatch(setIsPopUpLogin(true))}}>
          Đăng nhập</p>
         <hr/>
         <p>Cho thuê chỗ ở qua airbnb</p>
         <p>Tổ chức trãi nghiệm</p>
         <p>Trợ giúp</p>
         </UserStyled>
         }
       </div>
       </div>
   
  )
}

export default User